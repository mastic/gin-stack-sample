FROM golang:1.18-buster as builder
ARG app
ARG arch

WORKDIR /${app}
COPY . /${app}
RUN \
    make vars all

ENV USER=appuser
ENV UID=10001
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

# --------------------------------
FROM debian:buster-slim
ARG app
ARG arch
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder --chown=10001:10001 /${app}/bin/${app}-linux-${arch} /application
USER appuser:appuser
ENTRYPOINT [ "/application" ]

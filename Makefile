# mainly calls scripts from /scripts
include .env
MAKEFLAGS += --silent

.PHONY: help build test
## help: shows this help message
help: Makefile
	@echo
	@echo " Choose a command run in "$(PROJECTNAME)":"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo

## vars: prints build variables
vars:
	echo "PROJECTNAME : ${PROJECTNAME}"
	echo "MODULENAME  : ${MODULENAME}"
	echo "GOBASE      : ${GOBASE}"
	echo "GOBIN       : ${GOBIN}"
	echo "GOLIST      : ${GOLIST}"
	echo "STDERR      : ${STDERR}"
	echo "PID         : ${PID}"

# -- BUILD -----------------------------------------------

## all: build from scratch
all: go-compile
build: go-compile
go-compile: go-clean go-build

go-build:
	@go get
	@go mod verify
	@echo " .. Building binary..."
	GOOS=linux GOARCH=amd64 go build -ldflags "-w -s -extldflags ''" -o $(GOBIN)/$(PROJECTNAME) $(MAINGO)
	GOOS=linux GOARCH=amd64 go build -ldflags "-w -s -extldflags ''" -o $(GOBIN)/$(PROJECTNAME)-linux-amd64 $(MAINGO)
	GOOS=linux GOARCH=arm64 go build -ldflags "-w -s -extldflags ''" -o $(GOBIN)/$(PROJECTNAME)-linux-arm64 $(MAINGO)

go-clean:
	@echo " .. Cleaning build cache..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go clean

# -- RUN ------------------------------------------------

## stop: Stops development mode
stop: stop-server
restart-server: stop-server start-server

## start: Start development mode. Restarts when code changes
start:
	bash -c "trap 'make stop' EXIT; $(MAKE) compile start-server watch run='make compile start-server'"

start-server:
	@echo " .. $(PROJECTNAME) is available at $(ADDR)"
	@-$(GOBIN)/$(PROJECTNAME) 2>&1 & echo $$! > $(PID)
	@cat $(PID) | sed "/^/s/^/  \>  PID: /"

stop-server:
	@-touch $(PID)
	@-kill `cat $(PID)` 2> /dev/null || true
	@-rm $(PID)

watch:
	@go get github.com/azer/yolo
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) yolo -i . -e vendor -e bin -c "$(run)"

# -- TEST ------------------------------------------------

## test: Runs unit tests
test:
	@go get
	@-mkdir -p coverage
	@go test -race -v -coverpkg=./... -cover -coverprofile=coverage/covereage-tmp.out ./...
	cat coverage/covereage-tmp.out | grep -v "main.go" > coverage/covereage.out
	@go tool cover -html=coverage/covereage.out -o coverage/covereage.html
	@go tool cover -func=coverage/covereage.out | grep total:


## inspect: Runs source code inspections
inspect:
	@go fmt $(GOLIST)
	@go vet $(GOLIST)

# -- MODULE/DEPENDENCIES ------------------------------------------------

## tidy: tidies dependencies
tidy:
	@echo "tidying modules..."
	@go mod tidy

dependency-update:
	@echo "updating dependencies..."
	@go get -u -t
	@go mod tidy

## update: updates and tidies dependencies
update: tidy dependency-update

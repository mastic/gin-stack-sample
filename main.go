package main

import "gitlab.com/mastic/gin-stack-sample/app"

func main() {
	application := app.NewApplication()
	if err := application.Start(); err != nil {
		panic(err)
	}
}

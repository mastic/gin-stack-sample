package app

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewApplicationCreatesApplication(t *testing.T) {
	application := NewApplication()
	assert.NotEmpty(t, application)
	assert.False(t, application.IsRunning())
}

func TestGinStackSampleModelsCreatesModels(t *testing.T) {
	models := GinStackSampleModels()
	assert.NotEmpty(t, models)
	assert.Equal(t, 1, len(models))
}

package app

import (
	"gitlab.com/hd/lime"
	"gitlab.com/mastic/gin-stack-sample/products"
	"gorm.io/driver/sqlite"
)

func GinStackSampleModels() lime.DatabaseModels {
	return []lime.DatabaseModel{
		&products.Product{},
	}
}

func GinStackSampleDatabase() *lime.DatabaseConfiguration {
	return &lime.DatabaseConfiguration{
		DSN:     "file::memory:?cache=shared",
		Connect: sqlite.Open,
	}
}

func GinStackSampleRouter(ctx *lime.RuntimeContext) *lime.Router {
	database := ctx.Database
	routes := []lime.RoutedController{
		products.NewProductController(products.NewProductRepository(database)),
		// further controllers here
	}
	return &lime.Router{
		Routes: routes,
	}
}

func NewApplication() *lime.Application {
	return lime.ApplicationWith().
		Models(GinStackSampleModels()).
		Database(GinStackSampleDatabase()).
		Cors(GinCorsConfig()).
		Logging(GinLogConfig()).
		Router(GinStackSampleRouter).
		Configure()
}

func GinLogConfig() *lime.LogConfiguration {
	return &lime.LogConfiguration{
		GlobalLevel: lime.InfoLevel,
	}
}

func GinCorsConfig() *lime.CorsConfiguration {
	return &lime.CorsConfiguration{
		AllowOrigins: []string{
			"http://localhost:4200",
		},
	}
}

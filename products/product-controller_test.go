package products

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/kinbiko/jsonassert"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hd/lime"
	"gorm.io/driver/sqlite"
	"net/http"
	"testing"
)

func ProductControllerRouter(ctx *lime.RuntimeContext) *lime.Router {
	database := ctx.Database
	routes := []lime.RoutedController{
		NewProductController(NewProductRepository(database)),
	}
	return &lime.Router{
		Routes: routes,
	}
}

func CreateTestContext() *lime.TestContext {
	return lime.TestContextWith().
		Database(&lime.DatabaseConfiguration{
			DSN:     "file::memory:?cache=shared",
			Connect: sqlite.Open,
		}).
		Models([]lime.DatabaseModel{
			&Product{},
		}).
		Router(ProductControllerRouter).
		Configure()
}

func TestGetAll(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		req, err := http.NewRequest("GET", fmt.Sprintf("/products"), nil)
		assert.Empty(t, err)

		resp := testContext.Perform(req)

		assert.Equal(t, http.StatusOK, resp.Code)
		s := resp.Body.String()
		jsonassert.New(t).Assertf(s, `[]`)
	})
}

func TestGetOneMissing(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		req, err := http.NewRequest("GET", fmt.Sprintf("/products/%d", 404), nil)
		assert.Empty(t, err)

		resp := testContext.Perform(req)

		assert.Equal(t, http.StatusNotFound, resp.Code)
	})
}

func TestGetOneInvalid(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		req, err := http.NewRequest("GET", "/products/invalid!!", nil)
		assert.Empty(t, err)

		resp := testContext.Perform(req)

		assert.Equal(t, http.StatusBadRequest, resp.Code)
	})
}

func TestGetOne(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {

		entity := Product{
			Code:  "AAA",
			Price: 123,
		}
		err := context.CreateEntity(&entity)
		assert.Empty(t, err)

		req, err := http.NewRequest("GET", fmt.Sprintf("/products/%d", entity.ID), nil)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusOK, resp.Code)

		s := resp.Body.String()
		jsonassert.New(t).Assertf(s, `{
		"Code" : "%s",
		"Price": %d,
		"ID": "<<PRESENCE>>",
		"CreatedAt": "<<PRESENCE>>",
		"UpdatedAt": "<<PRESENCE>>",
		"DeletedAt": null
	}`, "AAA", 123)
	})
}

func TestDeleteOneMissing(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {

		req, err := http.NewRequest("DELETE", fmt.Sprintf("/products/%d", 404), nil)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusNotFound, resp.Code)
	})
}

func TestDeleteOneInvalid(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {

		req, err := http.NewRequest("DELETE", "/products/invalid", nil)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusBadRequest, resp.Code)
	})
}

func TestDeleteOne(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {
		entity := Product{
			Code:  "AAA",
			Price: 123,
		}
		err := context.CreateEntity(&entity)
		assert.Empty(t, err)

		req, err := http.NewRequest("DELETE", fmt.Sprintf("/products/%d", entity.ID), nil)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusOK, resp.Code)

		s := resp.Body.String()
		jsonassert.New(t).Assertf(s, `{
		"Code" : "%s",
		"Price": %d,
		"ID": %d,
		"CreatedAt": "<<PRESENCE>>",
		"UpdatedAt": "<<PRESENCE>>",
		"DeletedAt": null
	}`, "AAA", 123, entity.ID)

		// Check if entity is really deleted
		req, err = http.NewRequest("GET", fmt.Sprintf("/products/%d", entity.ID), nil)
		resp = context.Perform(req)

		assert.Empty(t, err)
		assert.Equal(t, http.StatusNotFound, resp.Code)
	})
}

func TestCreateOneInvalid(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {
		payload := new(bytes.Buffer)
		req, err := http.NewRequest("POST", "/products", payload)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusBadRequest, resp.Code)
	})
}

func TestCreateOne(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {

		body := &Product{
			Code:  "0001",
			Price: 42,
		}
		payload := new(bytes.Buffer)
		err := json.NewEncoder(payload).Encode(body)
		req, err := http.NewRequest("POST", "/products", payload)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusCreated, resp.Code)

		s := resp.Body.String()
		jsonassert.New(t).Assertf(s, `{
			"Code" : "%s",
			"Price": %d,
			"ID": "<<PRESENCE>>",
			"CreatedAt": "<<PRESENCE>>",
			"UpdatedAt": "<<PRESENCE>>",
			"DeletedAt": null
		}`, body.Code, body.Price)
	})
}

func TestUpdateOneMissing(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {

		body := &Product{
			Code:  "AAA-updated",
			Price: 1234,
		}
		payload := new(bytes.Buffer)
		err := json.NewEncoder(payload).Encode(body)

		req, err := http.NewRequest("PUT", fmt.Sprintf("/products/%d", 404), payload)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusNotFound, resp.Code)
	})
}

func TestUpdateOneInvalid(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {

		body := &Product{
			Code:  "AAA-updated",
			Price: 1234,
		}
		payload := new(bytes.Buffer)
		err := json.NewEncoder(payload).Encode(body)

		req, err := http.NewRequest("PUT", "/products/invalid", payload)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusBadRequest, resp.Code)
	})
}

func TestUpdateOneNoData(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {

		entity := Product{
			Code:  "AAA",
			Price: 123,
		}
		err := context.CreateEntity(&entity)
		assert.Empty(t, err)

		req, err := http.NewRequest("PUT", fmt.Sprintf("/products/%d", entity.ID), nil)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusBadRequest, resp.Code)
	})
}

func TestUpdateOne(t *testing.T) {
	context := CreateTestContext()
	context.RunTest(t, func(t *testing.T) {

		entity := Product{
			Code:  "AAA",
			Price: 123,
		}
		err := context.CreateEntity(&entity)
		assert.Empty(t, err)

		body := &Product{
			Code:  "AAA-updated",
			Price: 1234,
		}
		payload := new(bytes.Buffer)
		err = json.NewEncoder(payload).Encode(body)
		req, err := http.NewRequest("PUT", fmt.Sprintf("/products/%d", entity.ID), payload)
		assert.Empty(t, err)

		resp := context.Perform(req)

		assert.Equal(t, http.StatusOK, resp.Code)

		s := resp.Body.String()
		jsonassert.New(t).Assertf(s, `{
			"Code" : "%s",
			"Price": %d,
			"ID": %d,
			"CreatedAt": "<<PRESENCE>>",
			"UpdatedAt": "<<PRESENCE>>",
			"DeletedAt": null
		}`, body.Code, body.Price, entity.ID)
	})
}

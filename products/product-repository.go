package products

import "gorm.io/gorm"

type ProductRepository struct {
	db *gorm.DB
}

func NewProductRepository(db *gorm.DB) *ProductRepository {
	return &ProductRepository{db}
}

func (r *ProductRepository) GetAll() (list []Product, err error) {
	result := r.db.Find(&list)
	err = result.Error
	return list, err
}

func (r *ProductRepository) GetOne(id uint64) (product Product, err error) {
	result := r.db.First(&product, id)
	err = result.Error
	return product, err
}

func (r *ProductRepository) CreateOne(entity *Product) (product Product, err error) {
	result := r.db.Create(entity)
	err = result.Error
	return *entity, err
}

func (r *ProductRepository) UpdateOne(id uint64, entity *Product) (product Product, err error) {
	entity.ID = uint(id)
	result := r.db.Save(entity)
	err = result.Error
	return *entity, err
}

func (r *ProductRepository) DeleteOne(id uint64) (product Product, err error) {
	result := r.db.First(&product, id)
	err = result.Error
	if err != nil {
		return product, err
	}

	result = r.db.Delete(&Product{}, id)
	err = result.Error
	return product, err
}

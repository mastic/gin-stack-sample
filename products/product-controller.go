package products

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ProductController struct {
	repository *ProductRepository
}

func NewProductController(repository *ProductRepository) *ProductController {
	return &ProductController{
		repository: repository,
	}
}

func (ctrl *ProductController) Register(engine *gin.Engine) {
	endpoint := engine.Group("/products")
	{
		endpoint.GET("", ctrl.GetAll)
		endpoint.GET("/:id", ctrl.GetOne)
		endpoint.POST("", ctrl.CreateOne)
		endpoint.PUT("/:id", ctrl.UpdateOne)
		endpoint.DELETE("/:id", ctrl.DeleteOne)
	}
}

func (ctrl *ProductController) GetAll(c *gin.Context) {
	results, err := ctrl.repository.GetAll()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"Message": "Could not get entities", "error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, results)
}

func (ctrl *ProductController) GetOne(c *gin.Context) {
	id := c.Param("id")
	if id, err := strconv.ParseUint(id, 10, 64); err == nil {

		data, err := ctrl.repository.GetOne(id)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"Message": "entity not found", "error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, data)
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"Message": "Invalid parameter"})
	}
}

func (ctrl *ProductController) CreateOne(c *gin.Context) {
	var product Product

	if err := c.ShouldBindJSON(&product); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	data, err := ctrl.repository.CreateOne(&product)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError,
			gin.H{"Message": "entity cannot be saved", "error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, data)
}

func (ctrl *ProductController) UpdateOne(c *gin.Context) {
	id := c.Param("id")
	if id, err := strconv.ParseUint(id, 10, 64); err == nil {

		data, err := ctrl.repository.GetOne(id)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"Message": "entity not found", "error": err.Error()})
			return
		}

		var product Product

		if err := c.ShouldBindJSON(&product); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		data, err = ctrl.repository.UpdateOne(id, &product)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError,
				gin.H{"Message": "entity cannot be saved", "error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, data)
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"Message": "Invalid parameter"})
	}
}

func (ctrl *ProductController) DeleteOne(c *gin.Context) {
	id := c.Param("id")
	if id, err := strconv.ParseUint(id, 10, 64); err == nil {

		data, err := ctrl.repository.DeleteOne(id)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"Message": "entity not found", "error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, data)
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"Message": "Invalid parameter"})
	}
}
